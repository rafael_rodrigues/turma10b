package exemplos;

public class Exemplo01 {
    public static void main(String[] args) {
        
        int divisaoInteira;
        double divisaoReal;
        int sobra;

        divisaoInteira = 17 / 5;
        sobra = 17 % 5; // % calcula o RESTO da divisão
        divisaoReal = 17.0 / 5;

        System.out.println(divisaoInteira);
        System.out.println(sobra);
        System.out.println(divisaoReal);
    }
}
/*
 375 = 3 x 100 + 1 x 50 + 1 x 20 + 0 x 10 + 1 x 5 + 0 x 1
*/



package exemplos;

//Conversão de tipos

public class Exemplo02 {
    public static void main(String[] args) {
        
        int numeroInt;
        double numeroDouble;

        numeroInt = (int) 10.7418; //não é possível atribuir um valor MAIOR para um MENOR
        numeroDouble = 12;

        System.out.println(numeroInt);
        System.out.println(numeroDouble);
    }
}

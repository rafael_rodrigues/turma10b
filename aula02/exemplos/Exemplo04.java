package exemplos;

import java.util.Scanner;

public class Exemplo04 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        String nome;

        //nome = entrada.next(); //  lê até o espaço 
        nome = entrada.nextLine(); //  lê até o final da linha (ENTER) 

        System.out.println(nome);

        entrada.close();
    }
}

package exemplos;

import java.util.Scanner;

public class Exemplo06 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n1, n2, n3;

        n1 = in.nextInt();
        n2 = in.nextInt();
        n3 = in.nextInt();

        System.out.println(n1 + ","+ n2 + "," + n3);

        in.close();
    }
}

package exercicios;

import java.util.Scanner;

/*
1. ler os lados

- criar o scanner
- criar as variáveis
- usar o scanner para ler os 3 valores das variáveis

2. verificar se é triangulo

- verificar se cada lado é menor que a soma dos outros dois lados

3. classificar o triângulo

- verificar se todos os lados são iguais
- verificar se dois lados são iguais
--> se não for nenhum dos casos, todos diferentes

*/

public class Exercicio08 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int lado1, lado2, lado3;
        boolean ehTriangulo;

        System.out.println("Informe o valor dos Três lados:");
        lado1 = in.nextInt();
        lado2 = in.nextInt();
        lado3 = in.nextInt();

        ehTriangulo = (lado1 <= lado2 + lado3) && 
                    (lado2 <= lado1 + lado3) && 
                    (lado3 <= lado2 + lado1);

        if (ehTriangulo) {
            if (lado1 == lado2 && lado2 == lado3) {
                System.out.println("Triângulo equilátero.");
            } else {
                // (lado1 != lado2 && lado1 != lado3 && lado2 != lado3) - escaleno
                if (lado1 == lado2 || lado1 == lado3 || lado2 == lado3) {
                    System.out.println("Triângulo Isosceles.");
                } else {
                    System.out.println("Triângulo Escaleno.");
                }
            }
        } else {
            System.out.println("Não é um triângulo");
        }

        //ALT + SHIFT + SETA - copiar linha
        //ALT + SETA - mover linha
        
        in.close();
        
    }
}

package exercicios;

import java.util.Scanner;

public class Exercicio01 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int valor = in.nextInt();

        //boolean resposta = ehPar( valor );
        boolean resposta = isPar( valor );

        if ( resposta ) {
            System.out.println(valor + " é par");
        } else {
            System.out.println(valor + " é impar");
        }

        in.close();
    }

    static boolean isPar(int numero) {
        if (numero % 2 == 0) {
            return true;
        } else {
            return false;
        }
    }

    static boolean ehPar(int numero) {
        return (numero % 2 == 0);
    }

}

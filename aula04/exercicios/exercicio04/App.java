package exercicios.exercicio04;

public class App {
    public static void main(String[] args) {
        Relogio relogio = new Relogio(15, 12, 10);

        relogio.setHora(23);
        relogio.setMinuto(58);
        relogio.setSegundo(58);

        for (int i = 0; i < 10; i++) {
            relogio.avancaHora();
            System.out.println(relogio.getHoraAtual());

        }


    }

}

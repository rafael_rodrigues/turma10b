package exercicios.exercicio04;

public class Relogio {
    // atributos
    private int hora, minuto, segundo;

    public Relogio(int hora, int minuto, int segundo) {
        setHora(hora);
        setMinuto(minuto);
        setSegundo(segundo);
    }

    public String getHoraAtual() {
        return hora + ":" + minuto + ":" + segundo;
    }

    public void setHora(int hora) {
        if (hora > -1 && hora < 24) {
            this.hora = hora;
        }
    }

    public void setMinuto(int minuto) {
        if (minuto > 0 && minuto < 60) {
            this.minuto = minuto;
        }
    }

    public void setSegundo(int segundo) {
        if (segundo > 0 && segundo < 60) {
            this.segundo = segundo;
        }
    }

    public void avancaHora(){
        if(segundo < 59){
            segundo++;
        }else{
            segundo = 0;
            if (minuto < 59) {
                minuto++;
            } else {
                minuto = 0;
                if(hora < 23){
                    hora++;
                }else{
                    hora = 0;
                }
            }
        }
    }
}

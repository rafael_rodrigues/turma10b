package exemplos.exemplo04;

public class Pessoa {

    private String nome;
    private int idade;

    public Pessoa(String nome, int idade) {
        this.nome = nome;
        setIdade(idade);
    }

    // método para obter valor do atributo
    public String getNome() {
        return nome;
    }

    public int getIdade() {
        return idade;
    }

    // métodos para alterar valores dos atributos
    public void setIdade(int idade) {
        if(idade > this.idade){
            this.idade = idade;
        }
    }

}

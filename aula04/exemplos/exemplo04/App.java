package exemplos.exemplo04;

public class App {
    public static void main(String[] args) {

        Pessoa pessoa = new Pessoa("Jose", 23);

        // p.nome não pode ser acessado se for privado
        // pessoa.nome = "Emerson";

        System.out.println("Nome:" + pessoa.getNome());

        //pessoa.idade = 33;
        pessoa.setIdade(24);

        System.out.println("Idade:" + pessoa.getIdade());

    }
    
}

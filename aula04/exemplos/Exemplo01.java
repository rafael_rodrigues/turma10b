package exemplos;

public class Exemplo01 {

    public static void main(String[] args) {
        linha(); // chamada do método
        System.out.println("Bem vindo ao meu programa.");
        linha2(10);
        linha2(20); // chamada com parâmetro
        linha3(25, '*'); // chamada com 2 parâmetros (a ordem deve ser respeitada)
    }

    static void linha() { // método sem parâmetro, sem retorno (void)
        System.out.println("---------------------");
    }

    static void linha2(int tamanho) { // método com 1 parâmetro
        for (int i = 0; i < tamanho; i++) {
            System.out.print("-");
        }
        System.out.println();
    }

    static void linha3(int tamanho, char simbolo) { // método com 2 parâmetros
        for (int i = 0; i < tamanho; i++) {
            System.out.print(simbolo);
        }
        System.out.println();
    }

}
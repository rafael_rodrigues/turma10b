package exemplos;

public class Exemplo02 {
    
    public static void main(String[] args) {
        int valor;
        int a = 10; //variável local ao 'main'

        valor = soma(a, 15); // envia o valor de 'a' e o valor 15 para a 'soma'

        System.out.println("soma = " + valor);
    }

    //método 'soma' recebe dois inteiros como parâmetro, e retorna um inteiro
    static int soma(int n1, int n2){ //n1 e n2 só existem dentro da 'soma'
        int resultado;

        resultado = n1 + n2;

        return resultado; //devolve o valor calculado E TERMINA o método
    }
}

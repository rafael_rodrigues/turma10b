package exemplos.exemplo03;

//modelo para criar objetos do tipo Pessoa

public class Pessoa {

    // atributos: características
    String nome;
    int idade;

    //métodos: ações do objeto

    //método construtor - mesmo nome da classe
    public Pessoa(String nome, int idade){
        this.nome = nome; //this se refere ao próprio objeto
        this.idade = idade;
    }

    void apresentar(){
        System.out.println("Olá! Sou " + nome + " tenho " + idade);
    }

}

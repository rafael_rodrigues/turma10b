package exemplos.exemplo03;

public class App {
    public static void main(String[] args) {
        
        //cria um objeto do tipo Pessoa
        Pessoa pessoa1 = new Pessoa("Emerson", 11);
        Pessoa pessoa2 = new Pessoa("Amanda", 22);

        //preenche o atributo nome da pessoa1
        //pessoa1.nome = "Emerson";
        pessoa1.apresentar();
        
        //pessoa2.nome = "Amanda";
        pessoa2.apresentar();
    }

}

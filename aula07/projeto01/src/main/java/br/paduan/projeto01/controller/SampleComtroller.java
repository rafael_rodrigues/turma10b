package br.paduan.projeto01.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sample")
public class SampleComtroller {
    
    @GetMapping("/hello")
    public ResponseEntity<String> get(){
        return ResponseEntity.ok("Meu primeiro SpringBoot!");
    }
}

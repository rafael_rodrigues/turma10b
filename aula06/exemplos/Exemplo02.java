package exemplos;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Exemplo02 {
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        int numero;

        System.out.println("Digite um valor inteiro");

        try { // tente fazer isso
              // numero = Integer.parseInt(teclado.nextLine());
            numero = teclado.nextInt();
            System.out.println("O valor digitado:" + numero);
        } catch (InputMismatchException e) { // se ocorrer uma exceção faça isso
            System.out.println("Este não é um valor inteiro.");
            System.out.println(e.getMessage());
            e.printStackTrace(); // ver todas as chamadas onde o erro ocorreu
            numero = 0;
        } catch (NumberFormatException e) {
            System.out.println("Este não é um valor inteiro.");
            System.out.println(e.getMessage());
        } catch (Exception e) { // o tipo de exception genérico deve ser colocado no final
            System.out.println(e.getMessage());
        } finally { // sempre é executado, independente de ter feito o try ou algum catch
            teclado.close();
            System.out.println("Final do programa.");
        }

    }
}

package exemplos.exemplo01;

public class Gerente extends Funcionario {
    private int numFuncionarios;

    public Gerente(String nome, double salario, int numFuncionarios) {
        super(nome, salario); // Funcionario() - construtor
        this.numFuncionarios = numFuncionarios;
    }

    public void teste() {
        System.out.println(nome);
    }

    @Override
    public void aumentarSalario(double perc) {
        super.aumentarSalario(perc + 0.2);
    }

    @Override  //Anotação indicando sobrecarga de método da super classe 
    public void imprimir() {
        System.out.println("Nome: " + nome + " salario: " + getSalario() + " gerencia: " + numFuncionarios);
    }

    @Override
    public String toString() {
        return super.toString() + " gerencia: " + numFuncionarios;
    }

}

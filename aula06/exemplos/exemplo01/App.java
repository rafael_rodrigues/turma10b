package exemplos.exemplo01;

import java.util.ArrayList;

public class App {
    public static void main(String[] args) {
        ArrayList<Funcionario> listaFuncinarios = new ArrayList<>();

        Funcionario f = new Funcionario("Amanda", 4000);
        Gerente g = new Gerente("Carlos", 5000, 10);

        listaFuncinarios.add(f);
        listaFuncinarios.add(g);

        f.imprimir();
        g.imprimir();

        //f.aumentarSalario(0.1); //10% de acrescimo
        //g.aumentarSalario(0.1); //10% de acrescimo

        for (Funcionario item : listaFuncinarios) {  //para cada 'item' (Funcionário) da lista de Funcionarios faça:
            item.aumentarSalario(0.1);
        }

        //System.out.println( f );
        //System.out.println( g );

        for (Funcionario item : listaFuncinarios) {
            System.out.println( item );
        }

    }
}

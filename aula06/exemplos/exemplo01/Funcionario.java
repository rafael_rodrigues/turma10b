package exemplos.exemplo01;

public class Funcionario { //extends Object
    protected String nome; // protected funciona como public para as classes derivadas
    private double salario;

    public Funcionario(){ //construtor default

    }

    public Funcionario(String nome, double salario) {  //sobrecarga do construtor
        this.nome = nome;
        this.salario = salario;
    }

    public String getNome() {
        return nome;
    }

    public double getSalario(){
        return salario;
    }

    public void aumentarSalario(double perc){
        salario +=  salario * perc;
    }

    public void imprimir() {
        System.out.println("Nome: " + nome + " salario: " + salario);
    }

    @Override
    public String toString() {
        return "Nome: " + nome + " salario: " + salario;
    }

}
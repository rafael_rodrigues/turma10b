package exercicios.exercicio02;

public abstract class Conta { // Conta é uma classe abstrata
    private int numero;
    private double saldo;

    public Conta(int numero) {
        this.numero = numero;
        this.saldo = 0;
    }

    public int getNumero() {
        return numero;
    }

    public double getSaldo() {
        return saldo;
    }

    public boolean depositar(double valor) {
        if (valor > 0) {
            saldo += valor;
            return true;
        }
        return false;
    }

    //public abstract void sacar(double valor); // obriga toda classe derivada a implementar

    public boolean sacar(double valor) {
        if (valor > 0) {
            saldo -= valor;
            return true;
        }
        return false;
    }

    @Override // anotação que indica que o método toString da superclasse está sendo
              // sobrescrito
    public String toString() {
        return "Conta " + numero + " : " + saldo;
    }

}

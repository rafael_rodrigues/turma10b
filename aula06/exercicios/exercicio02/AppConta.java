package exercicios.exercicio02;

import java.util.Scanner;

public class AppConta {

    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        int opcao;
        int numeroConta;
        double limite, valor;

        GerenciaConta listaDeContas = new GerenciaConta();

        do {
            System.out.println("1-Nova Conta Corrente");
            System.out.println("2-Nova Conta Especial");
            System.out.println("3-Nova Conta Poupança");
            System.out.println("4-Realizar depósito");
            System.out.println("5-Realizar saque");
            System.out.println("6-Consultar saldo");
            System.out.println("7-Sair");
            System.out.print("==> ");
            opcao = teclado.nextInt();

            switch (opcao) {
                case 1:
                    System.out.println("Informe o número da conta:");
                    numeroConta = teclado.nextInt();
                    listaDeContas.novaContaCorrente(numeroConta);
                    break;

                case 2:
                    System.out.println("Informe o número da conta:");
                    numeroConta = teclado.nextInt();
                    System.out.println("Informe o limite da conta:");
                    limite = teclado.nextDouble();
                    listaDeContas.novaContaEspecial(numeroConta, limite);
                    break;

                case 3:
                    System.out.println("Informe o número da conta:");
                    numeroConta = teclado.nextInt();
                    listaDeContas.novaContaPoupanca(numeroConta);
                    break;

                case 4:
                    System.out.println("Informe o número da conta:");
                    numeroConta = teclado.nextInt();
                    System.out.println("Informe o valor: ");
                    valor = teclado.nextDouble();

                    boolean depositou = listaDeContas.depositar(numeroConta, valor);

                    if (depositou) {
                        System.out.println("Deposito realizado.");
                    } else {
                        System.out.println("Deposito não realizado.");
                    }

                    break;

                case 5:
                    System.out.println("Informe o número da conta:");
                    numeroConta = teclado.nextInt();
                    System.out.println("Informe o valor: ");
                    valor = teclado.nextDouble();

                    boolean sacou = listaDeContas.sacar(numeroConta, valor);

                    if (sacou) {
                        System.out.println("Saque realizado.");
                    } else {
                        System.out.println("Saque não realizado.");
                    }

                    break;

                case 6:
                    System.out.println("Informe o número da conta:");
                    numeroConta = teclado.nextInt();

                    System.out.println(listaDeContas.exibir(numeroConta));
                    break;

                case 7:
                    break;

                default: // nenhuma das outras opções
                    System.out.println("Opção inválida.");
                    break;
            }
        } while (opcao != 7);

        teclado.close();
    }

}

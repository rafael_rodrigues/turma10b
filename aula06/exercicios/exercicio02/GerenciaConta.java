package exercicios.exercicio02;

import java.util.ArrayList;

public class GerenciaConta {

    private ArrayList<Conta> contas = null;

    public GerenciaConta() {
        contas = new ArrayList<>();
    }

    public void novaContaCorrente(int numeroConta) {
        contas.add(new ContaCorrente(numeroConta));
    }

    public void novaContaEspecial(int numeroConta, double limite) {
        contas.add(new ContaEspecial(numeroConta, limite));
    }

    public void novaContaPoupanca(int numeroConta) {
        contas.add(new ContaPoupanca(numeroConta));
    }

    public boolean depositar(int numeroConta, double valor) {
        for (Conta item : contas) {
            if (item.getNumero() == numeroConta) {
                return item.depositar(valor);
            }
        }
        return false; // não achou a conta
    }

    public boolean sacar(int numeroConta, double valor) {
        for (Conta item : contas) {
            if (item.getNumero() == numeroConta) {
                return item.sacar(valor);
            }
        }
        return false; // não achou a conta
    }

    public String exibir(int numeroConta){
        for (Conta item : contas) { // para cada 'item' do tipo Conta no array de 'contas'
            if (item.getNumero() == numeroConta) {
                return item.toString();
            }
        }
        return "Conta inválida";
    }

}

package exercicios.exercicio02;

public class ContaPoupanca extends Conta {
    private static double taxa;  // define 'taxa' como sendo um atributo comum a todos os objetos

    public ContaPoupanca(int numero) {
        super(numero);
    }

    public static void setTaxa(double taxaPoupanca) {
        if (taxaPoupanca > 0) {
            taxa = taxaPoupanca;
        }
    }

    public static double getTaxa() {
        return taxa;
    }

    @Override
    public boolean sacar(double valor) {
        if(valor + taxa <= getSaldo()){
            return super.sacar(valor + taxa);
        }
        return false;
    }

}

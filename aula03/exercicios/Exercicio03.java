package exercicios;

import java.util.Scanner;

public class Exercicio03 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int limite, cont;

        System.out.print("Informe o limite: ");
        limite = in.nextInt();

        // *** Solução 1 - sem as virgulas ***

        cont = 1;
        
        while (cont <= limite) {
            System.out.print(cont + " ");
            cont *= 2; // isso é o mesmo que: cont = cont * 2;
        }
        System.out.println();

        // *** Solução 2 usando as vírgulas, testando (if) a cada valor ***

        cont = 1;
        while (cont <= limite) {
            if (cont <= limite / 2) {
                System.out.print(cont + ", ");
            } else {
                System.out.println(cont);
            }
            cont *= 2;
        }

        /* Solução 3 - sem if */

        cont = 1;
        while (cont <= limite / 2) {
            System.out.print(cont + ", ");
            cont *= 2;
        }
        System.out.println(cont);


        // *** Solução 4 - sem realizar teste (if), mas assume pelo menos 1 valor ***

        cont = 2;
        System.out.print(1);
        while (cont <= limite) {
            System.out.print(", " + cont);
            cont *= 2;
        }

        in.close();
    }
}
package exemplos;

public class Exemplo01 {
    public static void main(String[] args) {
        int cont, soma;

        soma = 0;
        cont = 0; //inicialização

        while ( cont < 10 ) { //limite da repetição
            System.out.println(cont + " + ");
            soma = soma + cont;
            cont++;  //passo da repetição  
            //cont--;
            //cont = cont + 2;
        }

        System.out.println(" = " + soma);
    }
    
}
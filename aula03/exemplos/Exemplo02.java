package exemplos;

/*

Operadores unários: ++ / --
soma ou subtrai 'um' de uma variável

Operadores contraídos (abreviados): +=  -=  /=  *=

cont = cont + 5;  //soma 5 ao valor da variável 'cont'
cont += 5;

*/
public class Exemplo02 {
    public static void main(String[] args) {
        int cont;

        cont = 5;
        cont++;  //cont = cont + 1;
        System.out.println(cont);
        
        cont += 5; //soma 5 ao valor da variável
        System.out.println(cont);

    }
}

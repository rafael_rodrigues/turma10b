package exemplos;

import java.util.ArrayList;

public class Exemplo01 {

    public static void main(String[] args) {
        //cria uma lista dinâmica para armazenar numeros inteiros
        ArrayList<Integer> lista = new ArrayList<>();

        lista.add(15);
        lista.add(5);
        lista.add(1);
        lista.add(20);
        lista.add(2, 33); //adiciona na terceira posição

        System.out.println("Qtde de itens: " + lista.size());

        System.out.println("Primeiro elemento da lista: " + lista.get(0));
        
        for (int i = 0; i < lista.size(); i++) {
            System.out.print(lista.get(i) + " ");
        }
        
        System.out.println();

        for (Integer item : lista) {
            System.out.print( item  + " ");
        }

        System.out.println();

        System.out.println( lista );

    }

}
package exemplos;

import java.util.HashSet;

public class Exemplo03 {
    public static void main(String[] args) {
        HashSet<String> carros = new HashSet<>();
        
        carros.add("Fusca");
        carros.add("Opala");
        carros.add("Variante");
        carros.add("Brasilia");
        carros.add("AAAA");
        carros.add("BBAA");
        carros.add("CCAA");
        

        System.out.println( carros );

    }
}

package exemplos;

import java.util.HashMap;

public class Exemplo02 {
    public static void main(String[] args) {
        HashMap<Integer,String> clientes = new HashMap<>();

        clientes.put(123, "João");
        clientes.put(456789, "Marcos");
        clientes.put(2222, "Mateus");
        clientes.put(789, "Sara");
        //clientes.put(789, "Emerson");


        System.out.println( "Cliente 789: " + clientes.get(789));

        System.out.println( clientes );
    }
}

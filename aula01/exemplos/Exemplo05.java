package exemplos;

import java.util.Scanner;

public class Exemplo05 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int numero;

        System.out.println("Digite um valor inteiro:");
        numero = entrada.nextInt(); //lê um valor inteiro do teclado e armazena na variável 'numero'

        System.out.println(numero);

        entrada.close();
    }
}

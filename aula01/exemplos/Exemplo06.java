package exemplos;

public class Exemplo06 {
    public static void main(String[] args) {
        String nome = "Carlos";
        int idade = 34;
        double peso = 85.321;


        System.out.println(nome + ", " + idade + " : " + peso);
        System.out.println("Nome: " + nome + ", idade: " + idade + ", peso: " + peso);
        
        //printf(FORMATO, VARIÁVEIS/VALORES)
        // s - string, d - inteiros, f - numeros com decimal       
        System.out.printf("Nome: %s, idade: %d, peso: %.2f\n" , nome, idade, peso);
    }
}

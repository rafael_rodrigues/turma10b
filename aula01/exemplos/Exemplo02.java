package exemplos;

public class Exemplo02 {
    
    public static void main(String[] args) {
        
        System.out.println("Um texto qualquer."); //println - pula para próxima linha]
        System.out.println("Outro\ntexto\nqualquer."); // \n pula linha

        System.out.print("Um texto "); //print - não pula para próxima linha
        System.out.print("seguido de outro texto");

    }
}

package exemplos; //indica a pasta onde a classe foi criada, a partir da raiz do projeto

/**
 * Exemplo01
 * 
 * @author: Emerson Paduan
 */

/*
 * Este é um comentário de bloco. Envolve várias linhas de código
 */

// Este é um comentário de linha

public class Exemplo01 {

   public static void main(String[] args) {
      // atalho: syso
      System.out.println("Meu primeiro programa!"); //escreva na tela esta frase

   }

}
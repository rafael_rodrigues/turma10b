package exemplos;

public class Exemplo04 {
    public static void main(String[] args) {
        
        int numero; //cria uma variável para armazenar um valor inteiro

        numero = 10;  //armazena dentro da variável 'numero' o valor 10;

        System.out.println(numero);
        
        numero = 5; //substituindo o valor por 5
        
        System.out.println(numero);
        
        System.out.println(numero + 7);

        System.out.println(numero);

        numero = 4 - numero; //avalia a expressão dolado direito e armazena na variável à esquerda.

        System.out.println(numero);
    }
}

package exercicios;

/*
    Exibir o resultado das 4 operações aritméticas básicas: + - * /
    entre os valores constantes 15 e 7
*/

public class Exercicio01 {
    public static void main(String[] args) {
        System.out.println("Resposta: " + (15 + 7));
        System.out.println("Resposta: " + (15 - 7));
        System.out.println("Resposta: " + (15 * 7));
        System.out.println("Resposta: " + (15 / 7)); //qdo os valores são inteiros, a resposta é inteira
        System.out.println("Resposta: " + (15.0 / 7));
    }
}

function validaLogin() {
    let userTxt = localStorage.getItem("userLogged");

    if (!userTxt) {
        window.location = "index.html";
    }

}

function logout() {
    localStorage.removeItem("userLogged");
    window.location = "index.html";
}

function voltar() {
    window.location = "dashmenu.html";
}

function gerarRelatorioAlarmes() {
    let dataini = document.getElementById("dtinicio").value;
    let datafim = document.getElementById("dtfinal").value;

    let dataMsg = {
        dt1: dataini,
        dt2: datafim
    }

    let cabecalho = {
        method: 'POST',
        body: JSON.stringify(dataMsg),
        headers: {
            'Content-type': 'application/json'
        }
    }

    fetch("http://localhost:8080/evento/alarmes", cabecalho)
        .then(res => res.json()) //extrai os dados do retorno 
        .then(result => preencheAlarmes(result));  //result é o resultado da extração

}

function preencheAlarmes(dados) {
    let tabela = '<table class="table table-sm"> <tr> <th>Alarme</th> <th>Quantidade</th> </tr>';

    for( i = 0; i < dados.length; i++){
        tabela = tabela + `<tr> 
            <td> ${dados[i][0]} </td> 
            <td> ${dados[i][1]} </td> 
        </tr>`;
    }

    tabela = tabela + '</table>';
    document.getElementById("tabela").innerHTML = tabela;

}



import { soma } from "./util.js";

export function validaLogin() {
    let userTxt = localStorage.getItem("userLogged");
    console.log(soma(1,2));
    if (!userTxt) {
        window.location = "index.html";
    }

}

export function logout() {
    localStorage.removeItem("userLogged");
    window.location = "index.html";
}

export function voltar() {
    window.location = "dashmenu.html";
}

export function gerarRelatorioEventos() {
    let dataini = document.getElementById("dtinicio").value;
    let datafim = document.getElementById("dtfinal").value;

    let dataMsg = {
        dt1: dataini,
        dt2: datafim
    }

    let cabecalho = {
        method: 'POST',
        body: JSON.stringify(dataMsg),
        headers: {
            'Content-type': 'application/json'
        }
    }

    fetch("http://localhost:8080/evento/data", cabecalho)
        .then(res => res.json()) //extrai os dados do retorno 
        .then(result => preencheAlarmes(result));  //result é o resultado da extração

}

export function preencheAlarmes(dados) {
    let options = {year: 'numeric', month: 'numeric', day: 'numeric' }
    let tabela = '<table class="table table-sm"> <tr> <th>Data</th> <th>Alarme</th> <th>Equipamento</th> </tr>';

    for(let i = 0; i < dados.length; i++){
        tabela = tabela + `<tr> 
                                <td> ${new Date(dados[i].dataevt).toLocaleString("pt-BR", options)} </td>
                                <td> ${dados[i].alarme.nome} </td>
                                <td> ${dados[i].equipamento.hostname} </td>
                            </tr>`
    }

    tabela = tabela + '</table>';
    document.getElementById("tabela").innerHTML = tabela;
}

